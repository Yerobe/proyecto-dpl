var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var favicon = require('serve-favicon');
const passport = require("./config/passport");
const session = require("express-session");

const store = new session.MemoryStore;

var jwt = require("jsonwebtoken");


var vueloRouter = require('./routes/vuelos');
var vuelosAPIRouter = require('./routes/api/vuelos');

var mongoose = require('mongoose');
var app = express();
var authAPIRouter = require('./routes/api/auth');

var usuariosRouter = require("./routes/usuarios");
var tokenRouter = require("./routes/tokens");




app.set('secretkey','JWT_PWD_!!223344');

/* ====== MONGOOSE ====== */

mongoose.connect('mongodb://127.0.0.1/Proyect', { useUnifiedTopology: true, useNewUrlParser: true });

mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on("error", console.error.bind('Error de conexión con MongoDB')); // Caso de Error




app.use(session({
  cookie: {magAge: 240*60*60*1000}, //Tiempo en milisegundos
  store: store,
  saveUninitialized: true,
  resave: "true",
  secret: "cualquier cosa no pasa nada 477447"
 
 }));





// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(passport.initialize());
app.use(passport.session());


app.use(express.static(path.join(__dirname, 'public')));

app.use(favicon(__dirname + '/public/images/favicon.ico'));



app.use('/', vueloRouter);
app.use('/vuelos', loggedIn, vueloRouter);

app.use("/token", tokenRouter);
app.use("/usuarios", loggedIn, usuariosRouter);

app.use('/api/auth', authAPIRouter);

app.use('/api/vuelos', validarUsuario, vuelosAPIRouter);


var Usuario = require("./models/Usuario");
var Token = require("./models/Token");






/* Usuarios - Login */


app.get("/login", function (req, res) { // Método GET

  res.render("session/login"); // Redireccionamiento

});



app.post("/login", function (req, res, next) {

  passport.authenticate("local", function (err, usuario, info) {

    if (err) return next(err);

    if (!usuario) return res.render("session/login", { info });

    req.logIn(usuario, function (err) {

      if (err) return next(err); // Caso de Error

      return res.redirect("/"); // En caso de Logueo Existoso, redireccionamiento

    });

  })(req, res, next);

});



app.get("/logout", function (req, res) {

  req.logOut(); //Limpiamos la sesión

  res.redirect("/"); // Redireccionamiento

});



app.get("/forgotPassword", function (req, res) {

  res.render("session/forgotPassword"); // Redireccionamiento a la Vista

});





app.post("/forgotPassword", function (req, res) { // 

  Usuario.findOne({ email: req.body.email }, function (err, usuario) { // Comprobación del Correo en Usuarios

    if (!usuario) return res.render("session/forgotPassword", { info: { message: "No existe ese email en nuestra BBDD." } }); // Mensaje de error


    usuario.resetPassword(function (err) { // Función de Reseteo

      if (err) return next(err);

      console.log("session/forgotPasswordMessage");

    });

    res.render("session/forgotPasswordMessage"); // Redireccionamiento 

  });

});



app.get("/resetPassword/:token", function (req, res, next) { // Función la cual se activa mediante la URL del mensaje del usuario

  Token.findOne({ token: req.params.token }, function (err, token) { // Comrpobamos la existencia del Token

    if (!token) return res.status(400).send({ type: "not-verified", msg: "No existe un usuario asociado al token. Verifique que su token no haya expirado." }); // Mensjae de Error



    Usuario.findById(token._userId, function (err, usuario) { // Filtrar al Usuario

      if (!usuario) return res.status(400).send({ msg: "No existe un usuario asociado al token." }); // Mensaje de error

      res.render("session/resetPassword", { errors: {}, usuario: usuario });  // Redireccionamiento

    });

  });

});



app.post("/resetPassword", function (req, res) {

  if (req.body.password != req.body.confirm_password) { // Si no coinciden las contraseñas dispuestas

    res.render("session/resetPassword", {
      errors: { confirm_password: { message: "No coincide con el password introducido." } }, // Mensajes de Error

      usuario: new Usuario({ email: req.body.email })
    }); // Instancia del nuevo Objeto, donde email: email

    return;

  }

  Usuario.findOne({ email: req.body.email }, function (err, usuario) { // Filtro por Email

    usuario.password = req.body.password, // Coincidencia de Contraseña

      usuario.save(function (err) {

        if (err) {

          res.render("session/resetPassword", { errors: err.errors, usuario: new Usuario({ email: req.body.email }) });

        } else {

          res.redirect("/login"); // Redireccionamiento

        }

      });

  });

});








// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




function loggedIn(req,res,next) {

  if (req.user) {
 console.log("Usuario Logueado con Exito");
  next();
 
  } else {
 
  console.log("Usuario no logueado");
 
  res.redirect("/login"); // Redireccionamiento
 
  }
 
 }



 function validarUsuario(req, res, next) {

  jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function(err, decoded){
 
  if (err) {
 
  res.json({status:"error", message: err.message, data:null});
 
  } else {
 
  req.body.userId = decoded.id;
 
  console.log('jwt verify: ' + decoded);
 
  next();
 
  }
 
 });

}



module.exports = app;
