let mongoose = require("mongoose"); // Importacion
let Schema = mongoose.Schema; // Esquemas
let Token = require("./Token");

let bcrypt = require("bcrypt"); // Nos permite la coddificación de las contraseñas
let uniqueValidator = require("mongoose-unique-validator"); // Nos permite realizar verificaciones antes de los guardados

let mailer = require("../mailer/mailer");

let saltRounds = 10; // SaltRounds, nos estamos refiriendo al factor de coste, es decir, ese controla cuánto tiempo se necesita para calcultar un solo hash de Bcrypt.
// Es por ello, que mientras mayor sea este número, se realizarán un mayor número de rondas, por lo que si incrementamos este, se duplica el tiempo necesario, y esto
// va ligado a que mientras mayor sea el tiempo que se requiere, resulta más complicado realizar un ataque por fuerza bruta.


const crypto = require('crypto');


let validateEmail = function (email) {

    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/; // Cambio 2,4

    return re.test(email);

}

let usuarioSchema = new Schema({


    nombre: {

        type: String,

        trim: true,

        required: [true, "El nombre es obligatorio"]

    },

    email: {

        type: String,

        trim: true,

        required: [true, "El email es obligatorio"],

        lowercase: true,

        unique: true,

        validate: [validateEmail, "Por favor, introduzca un email válido"],

        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/]

    },

    password: {

        type: String,

        required: [true, "El password es obligatorio"]

    },

    passwordResetToken: String,

    passwordResetTokenExpires: Date,

    verificado: {

        type: Boolean,

        default: false

    }


});




usuarioSchema.plugin(uniqueValidator, { message: "El email ya existe con otro usuario." });








/* Encriptamiento de la Constraseña */

usuarioSchema.pre("save", function (next) { // El método Pre nos permite que las funciones previas al middleware se ejecutan una tras otra, cuando cada middleware llama next

    if (this.isModified("password")) {

        this.password = bcrypt.hashSync(this.password, saltRounds);

    }

    next();

});


/* Comparación del Hash */


usuarioSchema.methods.validPassword = function (password) {

    return bcrypt.compareSync(password, this.password);

}





usuarioSchema.methods.enviar_email_bienvenida = function (cb) {

    let token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString("hex") }); //El token es un String en hexadecimal

    let email_destination = this.email;


    token.save(function (err) {

        if (err) { return console.log(err.message); }



        let mailOptions = {

            from: "no-reply@redbicicletas.com",

            to: email_destination,

            subject: "Verificación de cuenta",

            text: "Hola,\n\n" + "Por favor, para verificar su cuenta haga click en este enlace: \n" + "http://192.168.20.42:3000" + "\/token/confirmation\/" + token.token

        };

        


        mailer.sendMail(mailOptions, function (err) {

         

            if (err) { console.log("error"); }else{

            console.log("Se ha enviado un email de bienvenida a " + email_destination + ".");
            }
        });

    });

};



/* Mostrar Todos los Usuario */

usuarioSchema.statics.allUsuarios = function (cb) {
    return this.find({}, cb);
};


/* Crear nuevo Usuario */

usuarioSchema.statics.add = function (aUsuario, cb) { // Nos agrega una nueva Bicicleta - coolback
    return this.create(aUsuario, cb);
};

/* Borrar Usuario */

usuarioSchema.statics.removebyId = function (aUsuario, cb) { // Nos elimina un registro especificando ID
    return this.remove({ _id: aUsuario.UsuarioID }, cb);

};









usuarioSchema.methods.resetPassword = function(cb) {

    let token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString("hex")}); //El toke es un String en hexadecimal
   
    let email_destination = this.email;
   
    token.save(function(err){
   
    if (err) {return cb(err); }
   
   
   
    let mailOptions = {
   
    from: "no-reply@redbicicletas.com",
   
    to: email_destination,
   
    subject: "Reseteo de password de cuenta",
   
    text: "Hola,\n\n" + "Por favor, para resetear el password de su cuenta haga click en este enlace: \n" + "http://192.168.56.101:3000" + "\/resetPassword\/" + token.token + ".\n"
   
    };
   
   
   
    mailer.sendMail(mailOptions, function(err) {
   
    if (err) {return cb(err);}
   
   
   
    console.log("Se ha enviado un email para resetear el password a: " + email_destination + ".");
   
   
   
    });
   
    cb(null);
   
    });
   
   };


module.exports = mongoose.model("Usuario", usuarioSchema);
