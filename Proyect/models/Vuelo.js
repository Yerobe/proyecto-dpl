let mongoose = require("mongoose"); // Importacion
let Schema = mongoose.Schema; // Esquemas

let vueloSchema = new Schema({
 NumeroVuelo: String,
 Companie: String,
 HoraOriginal: Date,
 HoraEstimada: Date,
 Destino: String,
 Puerta: Number,
 Observation: String
});



vueloSchema.statics.allVuelosFiltro = function (req,cb) { // Nos muestra todos los Vuelos con Filtro para la API


    var min = req.body.min;
    var max = req.body.max;

    return this.find({HoraOriginal: {"$gte": new Date(String(min)), "$lt": new Date(String(max))}}, cb);
};




vueloSchema.statics.allVuelos = function (cb) { // Nos muestra todos los Vuelos

    var f = new Date();

    var min = f.getFullYear() + "-" + (f.getMonth()+1) + "-" + f.getDate();
    var max = f.getFullYear() + "-" + (f.getMonth()+1) + "-" + (f.getDate()+1);

    return this.find({HoraOriginal: {"$gte": new Date(String(min)), "$lt": new Date(String(max))}}, cb);
};


vueloSchema.statics.add = function(aVuelo, cb) { // Nos agrega una nueva Bicicleta - coolback
    return this.create(aVuelo, cb);
};


vueloSchema.statics.findById = function(aVuelo,cb) { // Nos filtra especificando un ID
  
    return this.findOne({_id: aVuelo},cb);

};


vueloSchema.statics.updateById = function(aVuelo,Id,cb) { // Nos elimina un registro especificando ID

 
    return this.updateOne({_id: Id},{$set: {NumeroVuelo: aVuelo.NumeroVuelo , Companie: aVuelo.Companie , HoraOriginal: aVuelo.HoraOriginal, HoraEstimada: aVuelo.HoraEstimada, Destino: aVuelo.Destino, Puerta: aVuelo.Puerta, Observation: aVuelo.Observation}}, cb);

};




vueloSchema.statics.removebyId = function(aVuelo, cb) { 
    
        return this.remove({_id: aVuelo},cb);

};






















module.exports = mongoose.model ("Vuelo", vueloSchema);


