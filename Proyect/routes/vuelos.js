var express = require('express'); // Incluimos Express
var router = express.Router(); // Hacemos uso la utilidad Router incluida en Express
let vueloController = require('../controllers/vuelo'); // Cargamos el controlador para el modelo

router.get("/", vueloController.vuelo_list);

router.get("/create", vueloController.vuelo_create_get);

router.post("/create", vueloController.vuelo_create_post);

router.get("/:id/update",vueloController.vuelo_update_get) 

router.post("/:id/update",vueloController.vuelo_update_post)

router.post("/:id/delete",vueloController.vuelo_delete_post)  // POST de eliminación al Id brindado




module.exports = router; // Exportamos el módulo para su accesibilidad general
