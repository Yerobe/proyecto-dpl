let express = require('express');
let router = express.Router();
let vueloControllerAPI = require("../../controllers/api/vueloControllerAPI");

router.get("/", vueloControllerAPI.vuelo_list);
router.get("/filtro", vueloControllerAPI.vuelo_list_filtro);

module.exports = router;


