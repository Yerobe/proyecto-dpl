let Usuario = require("../models/Usuario"); // Importación

module.exports = {
    list: function(req,res,next){     
     Usuario.find({}, function(err,usuarios) { // Búsqueda de los Usuarios
            if(err) res.send(500, err.message); // Caso de Error
            res.render("usuarios/index", {usuarios: usuarios}); // Redireccionamiento monstrando todos los usuarios
        });
    },
    update_get: function(req,res, next) {
        Usuario.findById(req.params.id, function (err, usuario) { // Búsqueda especificando un atributo, em este caso el Identificador obtenido por URL
            res.render("usuarios/update", {errors:{}, usuario: usuario}); // Redireccionamiento Mostrando el Usuario Filtrado
        });
    },
    update: function(req, res, next){
        let update_values = {nombre: req.body.nombre}; // Introducimos en variable el valor del Body
        Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario) { // Actualizamos por el Id en la URL y los nuevos cambios en la variable del Body
            if (err) { // Si existe Error
                console.log(err); // Imprimir en Terminal
                res.render("usuario/update", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})}); // Redireccionamiento con los detallas de este Usuario
            } else {
                res.redirect("/usuarios"); // Redireccionamiento a los Usuarios
                return;
            }
        });
    },
    create_get: function(req, res, next){
        res.render("usuarios/create", {errors:{}, usuario: new Usuario()}); // Redireccionamiento
    },
    create: function(req, res, next) {
        if (req.body.password != req.body.confirm_password) { // Si las contraseñas son diferentes
            res.render("usuarios/create", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return;
        }
        Usuario.create({nombre:req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsario) { // Crea el Usuario
            if(err){
                res.render("usuarios/create", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})}); // Caso de Error
            } else {
                nuevoUsario.enviar_email_bienvenida(); // Llamada a la función de Bienvenida
                res.redirect("/usuarios"); // Redireccionamiento
            }
        });
    },
    delete: function(req,res,next){
        Usuario.findByIdAndDelete(req.body.id, function(err){ // Elimina mediante Identificador
            if(err) // Caso de Error
                next(err);
            else   
                res.redirect("/usuarios"); // Redireccionamiento
        });
    }
};