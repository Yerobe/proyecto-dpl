let Vuelo = require("../models/Vuelo");

/* Definición del método */

exports.vuelo_list = function(req,res){

    Vuelo.allVuelos(function(err,vuelos){
        if(err) res.status(500).send(err.message);
        console.log(vuelos);
        
        res.render("index",{vuelos: vuelos});

    })
}


exports.vuelo_create_get = function(req,res){
    res.render("vuelos/create"); // Redirecciona al archivo pug que contiene el formulario con los atributos
}




exports.vuelo_create_post = function (req,res){
    
    let vuelo = new Vuelo({ 
        NumeroVuelo: req.body.NumeroVuelo,
        Companie: req.body.Companie,
        HoraOriginal: req.body.HoraOriginal,
        HoraEstimada: req.body.HoraEstimada,
        Destino: req.body.Destino,
        Puerta: req.body.Puerta,
        Observation: req.body.Observation
    });


    Vuelo.add(vuelo,function(err,vuelos){

        if(err) res.status(500).send(err.message);

        res.redirect("/");

    }); 

    
}



exports.vuelo_update_get = function(req,res) {

    Vuelo.findById(req.params.id,function(err,vuelo){ 
    if(err) res.status(500).send(err.message);
    res.render("vuelos/update", {vuelo}); 

});

}





exports.vuelo_update_post = function(req,res){
    
    let aVuelo = new Vuelo({ 
        NumeroVuelo: req.body.NumeroVuelo,
        Companie: req.body.Companie,
        HoraOriginal: req.body.HoraOriginal,
        HoraEstimada: req.body.HoraEstimada,
        Destino: req.body.Destino,
        Puerta: req.body.Puerta,
        Observation: req.body.Observation
    });

    Vuelo.updateById(aVuelo, req.params.id, function(err, NewVuelo){

        if(err) res.status(500).send(err.message);
        res.redirect("/"); 
    });




}




exports.vuelo_delete_post = function(req,res) { 



    Vuelo.removebyId(req.body.id,function(err,vuelos){

        if(err) res.status(500).send(err.message);
        
        res.redirect("/"); 

    })

   }








































